package com.kist.maitidevi

class Visitor {
	String firstName
	String lastName
	String email
	

    static constraints = {
		firstName blank:false, nullable:false, size:1..25
		lastName blank:false, nullable:false, size:1..25
		email email:true, blank:false, nulable:false 
		
    }
}
