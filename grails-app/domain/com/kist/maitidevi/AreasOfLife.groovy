package com.kist.maitidevi

class AreasOfLife {
	String label
	String color
	Integer value
	String highlight
	
    static constraints = {
		value inList:['1','2','3','4','5','6','7','8','9','10']
		
    }
}
