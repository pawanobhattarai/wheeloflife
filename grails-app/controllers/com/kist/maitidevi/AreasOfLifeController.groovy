package com.kist.maitidevi



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class AreasOfLifeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AreasOfLife.list(params), model:[areasOfLifeInstanceCount: AreasOfLife.count()]
    }

    def show(AreasOfLife areasOfLifeInstance) {
        respond areasOfLifeInstance
    }

    def create() {
        respond new AreasOfLife(params)
    }

    @Transactional
    def save(AreasOfLife areasOfLifeInstance) {
        if (areasOfLifeInstance == null) {
            notFound()
            return
        }

        if (areasOfLifeInstance.hasErrors()) {
            respond areasOfLifeInstance.errors, view:'create'
            return
        }

        areasOfLifeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'areasOfLife.label', default: 'AreasOfLife'), areasOfLifeInstance.id])
                redirect areasOfLifeInstance
            }
            '*' { respond areasOfLifeInstance, [status: CREATED] }
        }
    }

    def edit(AreasOfLife areasOfLifeInstance) {
        respond areasOfLifeInstance
    }

    @Transactional
    def update(AreasOfLife areasOfLifeInstance) {
        if (areasOfLifeInstance == null) {
            notFound()
            return
        }

        if (areasOfLifeInstance.hasErrors()) {
            respond areasOfLifeInstance.errors, view:'edit'
            return
        }

        areasOfLifeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'AreasOfLife.label', default: 'AreasOfLife'), areasOfLifeInstance.id])
                redirect areasOfLifeInstance
            }
            '*'{ respond areasOfLifeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(AreasOfLife areasOfLifeInstance) {

        if (areasOfLifeInstance == null) {
            notFound()
            return
        }

        areasOfLifeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AreasOfLife.label', default: 'AreasOfLife'), areasOfLifeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'areasOfLife.label', default: 'AreasOfLife'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
