<%@ page import="com.kist.maitidevi.AreasOfLife" %>



<div class="fieldcontain ${hasErrors(bean: areasOfLifeInstance, field: 'value', 'error')} required">
	<label for="value">
		<g:message code="areasOfLife.value.label" default="Value" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="value" from="${areasOfLifeInstance.constraints.value.inList}" required="" value="${fieldValue(bean: areasOfLifeInstance, field: 'value')}" valueMessagePrefix="areasOfLife.value"/>

</div>

<div class="fieldcontain ${hasErrors(bean: areasOfLifeInstance, field: 'color', 'error')} required">
	<label for="color">
		<g:message code="areasOfLife.color.label" default="Color" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="color" required="" value="${areasOfLifeInstance?.color}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: areasOfLifeInstance, field: 'highlight', 'error')} required">
	<label for="highlight">
		<g:message code="areasOfLife.highlight.label" default="Highlight" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="highlight" required="" value="${areasOfLifeInstance?.highlight}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: areasOfLifeInstance, field: 'label', 'error')} required">
	<label for="label">
		<g:message code="areasOfLife.label.label" default="Label" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="label" required="" value="${areasOfLifeInstance?.label}"/>

</div>

