
<%@ page import="com.kist.maitidevi.AreasOfLife" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'areasOfLife.label', default: 'AreasOfLife')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-areasOfLife" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-areasOfLife" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="value" title="${message(code: 'areasOfLife.value.label', default: 'Value')}" />
					
						<g:sortableColumn property="color" title="${message(code: 'areasOfLife.color.label', default: 'Color')}" />
					
						<g:sortableColumn property="highlight" title="${message(code: 'areasOfLife.highlight.label', default: 'Highlight')}" />
					
						<g:sortableColumn property="label" title="${message(code: 'areasOfLife.label.label', default: 'Label')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${areasOfLifeInstanceList}" status="i" var="areasOfLifeInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${areasOfLifeInstance.id}">${fieldValue(bean: areasOfLifeInstance, field: "value")}</g:link></td>
					
						<td>${fieldValue(bean: areasOfLifeInstance, field: "color")}</td>
					
						<td>${fieldValue(bean: areasOfLifeInstance, field: "highlight")}</td>
					
						<td>${fieldValue(bean: areasOfLifeInstance, field: "label")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${areasOfLifeInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
