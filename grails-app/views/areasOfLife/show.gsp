
<%@ page import="com.kist.maitidevi.AreasOfLife" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'areasOfLife.label', default: 'AreasOfLife')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-areasOfLife" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-areasOfLife" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list areasOfLife">
			
				<g:if test="${areasOfLifeInstance?.value}">
				<li class="fieldcontain">
					<span id="value-label" class="property-label"><g:message code="areasOfLife.value.label" default="Value" /></span>
					
						<span class="property-value" aria-labelledby="value-label"><g:fieldValue bean="${areasOfLifeInstance}" field="value"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${areasOfLifeInstance?.color}">
				<li class="fieldcontain">
					<span id="color-label" class="property-label"><g:message code="areasOfLife.color.label" default="Color" /></span>
					
						<span class="property-value" aria-labelledby="color-label"><g:fieldValue bean="${areasOfLifeInstance}" field="color"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${areasOfLifeInstance?.highlight}">
				<li class="fieldcontain">
					<span id="highlight-label" class="property-label"><g:message code="areasOfLife.highlight.label" default="Highlight" /></span>
					
						<span class="property-value" aria-labelledby="highlight-label"><g:fieldValue bean="${areasOfLifeInstance}" field="highlight"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${areasOfLifeInstance?.label}">
				<li class="fieldcontain">
					<span id="label-label" class="property-label"><g:message code="areasOfLife.label.label" default="Label" /></span>
					
						<span class="property-value" aria-labelledby="label-label"><g:fieldValue bean="${areasOfLifeInstance}" field="label"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:areasOfLifeInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${areasOfLifeInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
